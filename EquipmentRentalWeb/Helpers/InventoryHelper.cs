﻿using EquipmentRentalWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EquipmentRental.Helpers
{
   
    public static class InventoryHelper
    {
     
        public static IEnumerable<Equipment> GetInventoryFromAPI()
        {
            IEnumerable<Equipment> equipments = null;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Helpers.CommonHelper.BaseUrl);
                    //HTTP GET
                    var responseTask = client.GetAsync("Inventory");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<IList<Equipment>>();
                        readTask.Wait();

                        equipments = readTask.Result;
                    }
                    else
                    {
                        equipments = Enumerable.Empty<Equipment>();

                    }
                }
            }
            catch { throw; }
            return equipments;
    }
       
    }
}