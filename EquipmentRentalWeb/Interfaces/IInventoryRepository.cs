﻿
using System.Collections.Generic;

namespace EquipmentRentalWeb.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Inventory> GetAll();

        Product Get(int id);

        Product Add(Product item);

        bool Update(Product item);

        bool Delete(int id);
    }
}