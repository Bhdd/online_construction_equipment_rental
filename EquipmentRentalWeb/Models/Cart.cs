﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EquipmentRentalWeb.Models
{
    public class Cart 
    {
        // public Guid UserId { get; set; }
        private readonly List<CartItem> _items = new List<CartItem>();
        [JsonConstructor]
        public Cart(List<CartItem> items)
        {
            _items = items;
        }
        public Cart()
        {

        }
        public IList<CartItem> Items => _items;

        public void AddItem(CartItem cartItem)
        {
            if (!Items.Any(i => i.EquipmentItem.Id== cartItem.EquipmentItem.Id))
            {
                _items.Add(new CartItem()
                {
                    EquipmentItem = cartItem.EquipmentItem,
                    Days = cartItem.Days,
                    
                });
                return;
            }
            var existingItem = Items.FirstOrDefault(i => i.EquipmentItem.Id==cartItem.EquipmentItem.Id);
            existingItem.Days += cartItem.Days;
        }
    }
}
