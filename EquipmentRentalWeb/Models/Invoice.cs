﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquipmentRentalWeb.Models
{
    public class Invoice 
    {
        public string Title { get=>string.Format("Your invoice for the date:{0}", DateTime.Now.ToShortDateString()); }
        public Cart Cart { get; set; }    
        public int TotalPoint
        {
            get => (Cart.Items != null && Cart.Items.Count > 0) ? Cart.Items.Sum(x => x.Point) : 0;

        }
          public int TotalPrice
        {
            get => (Cart.Items != null && Cart.Items.Count > 0) ? Cart.Items.Sum(x => x.Price) : 0;

        }
        
    }
}
