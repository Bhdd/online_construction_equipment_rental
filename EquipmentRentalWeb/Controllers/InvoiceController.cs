﻿
using EquipmentRentalWeb.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace EquipmentRental.Controllers
{
    public class InvoiceController : Controller
    {
        
        public FileStreamResult CreateInvoice()
        {
            try
            {
                var cart = Session["Cart"] != null ? Session["Cart"] as Cart : null;
                if (cart != null)
                {
                    Invoice invoice = null;

                    using (var client = new HttpClient())
                    {
                    
                        client.BaseAddress = new System.Uri(Helpers.CommonHelper.BaseUrl);
                        var myContent = JsonConvert.SerializeObject(cart);

                        var stringContent = new StringContent(myContent);
                        stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        var responseTask = client.PostAsync("Invoice", stringContent);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<Invoice>();
                            readTask.Wait();

                            invoice = readTask.Result;
                            string invoiceString = "\nTitle:\r\n" + invoice.Title + "\r\n "+"Items:\r\n";
                            foreach (CartItem cartItem in invoice.Cart.Items)
                            {
                                invoiceString += "Equipment name:"+cartItem.EquipmentItem.Name + ", Equipment type: " + cartItem.EquipmentItem.EquipmentType + ", Number of days to rent: " + cartItem.Days.ToString() + ", Price: " + cartItem.Price.ToString() + ", Point: " + cartItem.Point.ToString() + "\r\n";
                            }
                            invoiceString += "Summary: \r\n" + "Total Price: " + invoice.TotalPrice + "\n" + "  Total Point: " + invoice.TotalPoint + "\r\n";

                            var byteArray = System.Text.Encoding.ASCII.GetBytes(invoiceString);
                            var stream = new System.IO.MemoryStream(byteArray);
                            return File(stream, "text/plain", "invoice.txt");
                        }

                    }

                }

                return null;
            }
            catch
            {
                throw;
            }
           
        }
    }
}
