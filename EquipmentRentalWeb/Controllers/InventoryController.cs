﻿using EquipmentRentalWeb.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EquipmentRental.Controllers
{
    public class InventoryController : Controller
    {

        private IEnumerable<Equipment> equipments = null;
        // GET: Inventory
       
        public InventoryController()
        {
            equipments = Helpers.InventoryHelper.GetInventoryFromAPI();
        }
        [OutputCache(Duration = 14400)] //cahce for 4 hours
        public ActionResult Index()
        {

            return View(equipments);
        }

        
        [OutputCache(Duration = 1440, VaryByParam = "id")]
        public ActionResult CartItem(int id)
        {
            var eq = equipments.FirstOrDefault<Equipment>(x => x.Id == id);
            var cartItem = new CartItem { Days = 0, EquipmentItem = eq, Point = 0, Price = 0 };


            return View(cartItem);
        }

      
        [HttpPost]
        public ActionResult CartItem(int id, CartItem cartItem)
        {
            try
            {
                var cart = (Session["Cart"]) != null ? Session["Cart"] as Cart : new Cart();
                cart.AddItem(cartItem);
                Session["Cart"] = cart;
                return RedirectToAction("Index","Cart");
            }
            catch
            {
                return View();
            }
        }

        
        public ActionResult Delete(int id)
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
