﻿using EquipmentRental.Interfaces;
using EquipmentRental.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EquipmentRental.Helpers;

namespace EquipmentRental.Service
{
    public class CartItemService : ICartItemService
    {
        public int CalculateCartItemPoint(CartItem cartItem) =>(cartItem.EquipmentItem.EquipmentType == EquipmentType.Heavy) ? 2 : 1;
       

        /// <summary>
        /// //rental price is one-time rental fee plus premium fee for each day rented.
        //Regular – rental price is one-time rental fee plus premium fee for the first 2 days plus regular fee for the number of days over 2.
        //Specialized – rental price is premium fee for the first 3 days plus regular fee times the number of days over 3.
        /// </summary>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        public int CalulateCartItemPrice(CartItem cartItem)
        {

            switch (cartItem.EquipmentItem.EquipmentType)
            {

                case EquipmentType.Heavy:
                    return Declerations.RentalPrice.GetValueOrDefault("One-time_Rental") + Declerations.RentalPrice.GetValueOrDefault("Premium_Rental") * cartItem.Days;
                case EquipmentType.Regular:
                    return (Declerations.RentalPrice.GetValueOrDefault("One-time_Rental") +
                        (cartItem.Days <= 2 ? Declerations.RentalPrice.GetValueOrDefault("Premium_Rental") * cartItem.Days:0) + 
                        (cartItem.Days > 2 ? Declerations.RentalPrice.GetValueOrDefault("Regular_Rental") * (cartItem.Days - 2) + Declerations.RentalPrice.GetValueOrDefault("Premium_Rental")*2 : 0));

                case EquipmentType.Specialized:
                    return ((cartItem.Days <= 3 ? (Declerations.RentalPrice.GetValueOrDefault("Premium_Rental") * cartItem.Days):0) + 
                       ( cartItem.Days > 3 ? Declerations.RentalPrice.GetValueOrDefault("Regular_Rental") * (cartItem.Days - 3)+ Declerations.RentalPrice.GetValueOrDefault("Premium_Rental") *3 : 0));
                default:return 0;
            }
                        
        }
    }
}
