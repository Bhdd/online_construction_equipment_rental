﻿using EquipmentRental.Interfaces;
using EquipmentRental.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquipmentRental.Service
{
    public class InvoiceService : IInvoiceService
    {
        private readonly ICartItemService _cartItemService;
        private readonly IInvoice _Invoice;
        private readonly ILogger _logger;
        public InvoiceService(ICartItemService cartItemservice,IInvoice invoice, ILogger<InvoiceService> logger)
        {
            _cartItemService = cartItemservice;
            _Invoice = invoice;
            _logger = logger;
        }
        public IInvoice GenerateInvoice(Cart cart)
        {
           
                foreach (CartItem cartItem in cart.Items)
                {
                    cartItem.Price = _cartItemService.CalulateCartItemPrice(cartItem);
                    cartItem.Point = _cartItemService.CalculateCartItemPoint(cartItem);

                }

                _Invoice.Cart = cart;
            _logger.LogInformation("Invoice created with Totla price:{0} and Total point:{1}", _Invoice.TotalPrice, _Invoice.TotalPoint);
            return _Invoice;
        }

       
    }
}
