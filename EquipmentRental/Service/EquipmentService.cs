﻿using EquipmentRental.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EquipmentRental.Models;

namespace EquipmentRental.Service
{
    public class EquipmentService : IEquipmentService
    {
        private IList<Equipment> _list = new List<Equipment> { };
        public IEnumerable<Equipment> GetEquipmentList()
        {
            var inventoryLines = System.IO.File.ReadAllLines(@".\data\Inventory.txt");
            foreach(string l in inventoryLines)
            {
                string []s = l.Split(Char.Parse("\t"));
                _list.Add(new Equipment { Id = int.Parse(s[0]), Name = s[1], EquipmentType = Enum.Parse<EquipmentType>( s[2]) });
            }
            return _list;
        }
    }
}