﻿namespace EquipmentRental.Models
{
    public class CartItem 
    {
      
        public int Days { get; set; }
        public Equipment EquipmentItem { get; set; }

        public virtual int Price { get; set; }
        public virtual int Point { get; set; }

    }
}
