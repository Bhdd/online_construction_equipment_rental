﻿using System;
using System.Collections.Generic;

namespace EquipmentRental.Helpers

{
    public static class Declerations
    {
      
        public static readonly Dictionary<String, int> RentalPrice = new Dictionary<string, int>() {
            { "One-time_Rental",100 },     { "Premium_Rental",60 }  ,{ "Regular_Rental",40 } 
        };
    }
}