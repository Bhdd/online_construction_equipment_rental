﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EquipmentRental.Interfaces;
using EquipmentRental.Service;
using EquipmentRental.Models;

namespace EquipmentRental
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddScoped<IEquipmentService, EquipmentService>();
            services.AddScoped<ICartItemService, CartItemService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddTransient<IInvoice, Invoice>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            //app.UseSession();

           // app.UseStaticFiles();
        }
    }
}
