﻿using EquipmentRental.Models;

namespace EquipmentRental.Interfaces
{
    public interface ICartItemService
    {
        int CalulateCartItemPrice(CartItem cartItem);
        int CalculateCartItemPoint(CartItem cartItem);
    }
}
