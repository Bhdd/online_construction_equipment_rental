﻿using EquipmentRental.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquipmentRental.Interfaces
{
    public interface IInvoiceService
    {
       IInvoice GenerateInvoice(Cart cart);
    }
}
