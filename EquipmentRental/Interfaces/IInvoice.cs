﻿using EquipmentRental.Models;

namespace EquipmentRental.Interfaces
{
    public interface IInvoice
    {
        string Title { get;  }
        Cart Cart { get; set; }
        int TotalPoint { get; }
        int TotalPrice { get; }
       
    }
}
