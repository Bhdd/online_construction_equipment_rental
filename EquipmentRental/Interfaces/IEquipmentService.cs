﻿using EquipmentRental.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquipmentRental.Interfaces
{
    public interface IEquipmentService
    {
      IEnumerable<Equipment> GetEquipmentList();
    }
}