﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EquipmentRental.Interfaces;
using EquipmentRental.Models;

namespace EquipmentRental.Controllers
{
    [Route("api/[controller]")]
    public class InventoryController : Controller
    {
        private IEquipmentService _inventoryService;
        public InventoryController(IEquipmentService InventoryService)
        {
            _inventoryService = InventoryService;

        }
        [HttpGet]
        public IEnumerable<Equipment> Get()
        {
            return _inventoryService.GetEquipmentList();
        }
      
    }
}
