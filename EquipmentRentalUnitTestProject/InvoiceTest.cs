﻿using EquipmentRental.Models;
using EquipmentRental.Service;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Xunit;

namespace EquipmentRentalUnitTestProject
{
    public class InvoiceTest
    {
        [Fact]
        public void InvoiceTotalPriceTest()
        {
           
            CartItem cartItem1 = new CartItem { Days = 3, EquipmentItem = new Equipment { EquipmentType = EquipmentType.Heavy, Id = 1, Name = "Caterpillar bulldozer" } };
            CartItem cartItem2 = new CartItem { Days = 5, EquipmentItem = new Equipment { EquipmentType = EquipmentType.Regular, Id = 2, Name = "KamAZ truck" } };
            Cart cart = new Cart();
            cart.AddItem(cartItem1);
            cart.AddItem(cartItem2);
             
            InvoiceService service = new InvoiceService(new CartItemService(), new Invoice() , new LoggerFactory().CreateLogger<InvoiceService>())  ;
            var invoic=service.GenerateInvoice(cart);
            Assert.Equal(620, invoic.TotalPrice);
            Assert.Equal(3, invoic.TotalPoint);
        }

       
    }
}
