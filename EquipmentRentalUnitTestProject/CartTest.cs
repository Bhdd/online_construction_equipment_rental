using EquipmentRental.Models;
using EquipmentRental.Service;
using System.Linq;
using Xunit;

namespace EquipmentRentalUnitTestProject
{
    public class CartTest
    {
        [Fact]
        public void CalulateCartItemPriceTest()
        {
           CartItem cartItem = new CartItem { Days = 3, EquipmentItem = new Equipment { EquipmentType = EquipmentType.Heavy, Id = 1, Name = "Caterpillar bulldozer" } };
           CartItemService service = new CartItemService();
           int point= service.CalculateCartItemPoint(cartItem);
           Assert.Equal(2, point);
           int price = service.CalulateCartItemPrice(cartItem);
           Assert.Equal(280, price);

        }

        [Fact]
        public void CardDuplicateItemTest()
        {
            CartItem cartItem = new CartItem { Days = 3, EquipmentItem = new Equipment { EquipmentType = EquipmentType.Heavy, Id = 1, Name = "Caterpillar bulldozer" } };
            CartItem cartItemD = new CartItem { Days = 8, EquipmentItem = new Equipment { EquipmentType = EquipmentType.Heavy, Id = 1, Name = "Caterpillar bulldozer" } };
            Cart cart = new Cart();
            cart.AddItem(cartItem);
            cart.AddItem(cartItemD);
            Assert.Equal(1, cart.Items.Count);//Items are the same but days count
            Assert.Equal(11, cart.Items.ToList().Sum(x => x.Days));


        }
    }
}
