Online construction equipment rental is a simple application which calculate rental prices according to equipment type and the number of days user selects.

This app has two tiers :

1.EquimentRental: back-end tier. All the bussines rules and web APIsare included in it.
2.EquipmentRentalWeb: front-end tier whic connet to back-end via web api. the base address of web api is set to: http://localhost:62731/api

I used xUnit to add unit test to asp.net core project and also Microsoft.Extension.Logging to try log application error in Application_Start, Alos add logger to asp.net core Invoice Service
I used Output cache when the list of equipments is loaded in front-end app. because this list is almost static I used caching to hold in memory.
